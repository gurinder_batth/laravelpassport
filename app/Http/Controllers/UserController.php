<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
     public function register(Request $request)
     {

     	 $request->validate([
          'name' => 'required' ,
          'password' => 'required' ,
          'email' => 'required|unique:users'
     	 ]);


		  $user = User::create([
		          'name' => $request->name ,
		          'password' => bcrypt($request->password) ,
		          'email' => $request->email 
		     	 ]);


		 $token = $user->createToken('token')->accessToken;

		 return response()->json($token);
     }

     public function profile(Request $request)
     {
     	return response()->json($request->user());
     }

     public function login(Request $request)
     {
     	$status = Auth::attempt([
     		'email' => $request->email ,
     		'password' => $request->password
     	]);

     	if($status){
     	 $token = Auth::user()->createToken('token')->accessToken;
		 return response()->json($token);
     	}

     	return response('unauth',401);

     }
}
